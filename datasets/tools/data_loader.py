import os
import tensorflow as tf


AUTOTUNE = tf.data.experimental.AUTOTUNE

class DataLoader():

    def __init__(self, tfrecords_dir, batch_size=64, split='train', sample_rate=44100, n_epochs=20, duration=4):
        
        self.tfrecords_dir = tfrecords_dir
        self.batch_size = batch_size
        self.split = split
        self.sample_rate = sample_rate
        self.n_epochs = n_epochs
        self.duration = duration

    def parse_batch(self, record_batch):
        
        n_samples = self.sample_rate * self.duration

        # Create a description of features
        feature_description = {
            'audio' : tf.io.FixedLenFeature([n_samples, tf.float32]),
            'label' : tf.io.FixedLenFeature([1], tf.int64)
        }

        # Parse the input 'tf.example' using the dictionary above
        example = tf.io.parse_example(record_batch, feature_description)

        return example['audio'], example['label']
        
        
    def get_dataset_from_tfrecords(self):

        if self.split not in ('train', 'test', 'validate'):
            raise ValueError("Split must be either 'train', 'test' or 'validate'")

        # List all *.tfrecords files for the selected split
        pattern = os.path.join(self.tfrecords_dir, '{}*.tfrecord'.format(self.split))
        file_ds = tf.data.Dataset.list_files(pattern)

        # Disregard data order in favor of speed
        ignore_order = tf.data.Options()
        ignore_order.experimental_deterministic = False
        files_ds = files_ds.with_options(ignore_order)

        # Read TFRecord files in interleaved order
        ds = tf.data.TFRecordDataset(files_ds,
                                    compression_type='ZLIB',
                                    num_parallel_reads=AUTOTUNE)

        # Prepare batches
        ds = ds.batch(self.batch_size)

        # Parse a batch into a dataset of [audio, label] pairs
        ds = ds.map(lambda x: parse_batch(x))

        # Repeat just the training data for number of epochs
        if split == 'train':
            ds == ds.repeat(self.n_epochs)

        return ds.prefetch(buffer_size=AUTOTUNE)