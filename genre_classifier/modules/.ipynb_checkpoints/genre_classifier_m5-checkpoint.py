from mel_spectrogram import MelSpectrogram
import tensorflow as tf
from tensorflow.keras.layers import (BatchNormalization, Conv2D, Dense,
                                     Dropout, Flatten, Input, MaxPool2D,
                                    ZeroPadding2D, GlobalMaxPooling2D, 
                                     Reshape, LSTM, TimeDistributed)
from tensorflow.keras.models import Model

def CNN_LSTM_Model(n_classes=8, sample_rate=22050, fft_size=2048, hop_length=512, n_mels=128, duration=10):
    
    n_samples = sample_rate * duration
    
    input_shape = (n_samples,)
    
    x = Input(shape=input_shape, name='input', dtype='float32')
    y0 = MelSpectrogram(sample_rate, fft_size, hop_length, n_mels, name='mel-spectrogram')(x)
    
    # Data normaization on frequency axis
    y = BatchNormalization(axis=2)(y0)

    y = ZeroPadding2D(padding=(1, 1), input_shape=input_shape)(y)
    y = Conv2D(32, 3, 3, activation='relu', kernel_initializer='glorot_uniform', name='block1_conv1')(y)
    y = BatchNormalization()(y)
    y = MaxPool2D((2, 2), strides=(1, 1) , name='block1_pool1')(y)

    y = Dropout(0.25)(y)
    
    y = ZeroPadding2D(padding=(1, 1))(y)
    
    conv2d = Conv2D(64, 3, 3, activation='relu', kernel_initializer='glorot_uniform', name='block2_conv1')
    y = TimeDistributed(conv2d)(y)
    
    y = BatchNormalization()(y)
    
    max_pool2d = GlobalMaxPooling2D()
    y = TimeDistributed(max_pool2d)(y)
    

    
    y = LSTM(256, activation='relu', 
              recurrent_dropout=0.3, kernel_initializer='glorot_uniform', 
              return_sequences=False, name='lstm1')(y)
    
    y = Dropout(0.5)(y)
    
    y = BatchNormalization()(y)
    y = Dense(1024, activation='relu', name='fc2')(y)
    
    y = Dropout(0.5)(y)
    
    y = BatchNormalization()(y)
    y = Dense(512, activation='relu', name='fc3')(y)
    
    
    y = Dropout(0.5)(y)
    y = Dense(n_classes, activation='softmax', name='prediction')(y9)
    
    return Model(inputs=x, outputs=y)