import tensorflow as tf
from tensorflow.keras.layers import (BatchNormalization, Conv2D, Dense,
                                     Dropout, Flatten, Input, MaxPool2D,
                                    ZeroPadding2D, GlobalMaxPooling2D, LSTM)
from tensorflow.keras.models import Model

def DNNModel(input_shape, n_classes=8):
    
    input_shape = input_shape
    
    x = Input(shape=input_shape, name='input', dtype='float32')
    
    y = BatchNormalization(input_shape=input_shape)(x)
    
    y = LSTM(512, input_shape=input_shape, activation='relu', 
              recurrent_dropout=0.3, kernel_initializer='glorot_uniform', 
              return_sequences=True, name='lstm1')(y)
    
    y = LSTM(512, activation='relu', 
              recurrent_dropout=0.3, kernel_initializer='glorot_uniform', 
              return_sequences=True, name='lstm2')(y)
    
    y = LSTM(512, activation='relu', 
              recurrent_dropout=0.3, kernel_initializer='glorot_uniform', 
              return_sequences=True, name='lstm3')(y)
    
    y = BatchNormalization()(y)
    
    y = Dense(1024, activation='relu', name='fc1')(y)
    
    y = Dropout(0.5)(y)
    
    y = Flatten()(y)
    
    y = Dense(n_classes, activation='softmax', name='prediction')(y)
    
    return Model(inputs=x, outputs=y)
    