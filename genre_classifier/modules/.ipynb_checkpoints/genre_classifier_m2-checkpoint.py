import tensorflow as tf
from tensorflow.keras.layers import (BatchNormalization, Conv2D, Dense,
                                     Dropout, Flatten, Input, MaxPool2D)
from tensorflow.keras.models import Model

def DNNModel(n_classes=8, n=20):
    
    input_shape = (n,)
    
    x = Input(shape=input_shape, name='input', dtype='float32')
    
    y1 = BatchNormalization()(x)
    
    y2 = Dense(1024, activation='relu', name='fc1')(y1)
    
    y3 = Dropout(0.25)(y2)
    
    y4 = Dense(2048, activation='relu', name='fc2')(y3)
    
    y5 = Dropout(0.25)(y4)
    
    y5 = Dense(1024, activation='relu', name='fc3')(y4)
    
    y6 = Dropout(0.25)(y5)
    
    y = Dense(n_classes, activation='softmax', name='prediction')(y6)
    
    return Model(inputs=x, outputs=y)
    