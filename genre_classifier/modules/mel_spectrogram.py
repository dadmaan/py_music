import tensorflow as tf
import librosa as lr
import numpy as np

class MelSpectrogram(tf.keras.layers.Layer):
    """ Compute log-magnitude mel-scaled spectrograms of audio data."""

    def __init__(self, sample_rate, fft_size, hop_length, n_mels, f_min=0.0, f_max=None, **kwargs):
        """ 
        This function initialize class parameters and variables.

        Parameters:
        sample_rate : The number of samples per second of the signal.
        fft_size : The size of FFT window to process the signal.
        hop_length : The number of samples in each segment (bin).
        n_mels : The number mel-frequency bins in output.
        num_spectrogram_bins : The number of spectrogram bins in the source spectrogram.
        f_min (Hertz) : The lowest frequency to include in mel-spectrogram.
        f_max (Hertz) : The highest frequency to include in mel-spectrogram.
        """
        super().__init__(**kwargs)
        self.sample_rate = sample_rate
        self.fft_size = fft_size
        self.hop_length = hop_length
        self.n_mels = n_mels
        self.f_min = f_min
        self.f_max = f_max if f_max else sample_rate/2
        # Preparing the mel spectrogram filter container
        self.mel_filterbank = tf.signal.linear_to_mel_weight_matrix(
                                num_mel_bins = self.n_mels,
                                num_spectrogram_bins = self.fft_size // 2 +1,
                                sample_rate = self.sample_rate,
                                lower_edge_hertz = self.f_min,
                                upper_edge_hertz = self.f_max)

    def build(self, input_shape):
        self.non_trainable_weights.append(self.mel_filterbank)
        super().build(input_shape)

    def call(self, waveforms):
        """ 
        This function takes input signal as a batch of mono waveforms and compute
        the corresponding batch of mel-spectrogram.

        Parameters:
        Waveforms : tf.Tensor with shape of (None, n_samples).

        Returns:
        mel-spectrogram : tf.Tensor with shape of (None, time, freq, ch).
        """

        def tf_log10(x):
            """ This function sacles the mel-spectrogram logarithmically."""

            numerator = tf.math.log(x)
            denominator = tf.math.log(tf.constant(10, dtype=numerator.dtype))
            return numerator/denominator

        def power_to_db(magnitude, amin=1e-16, top_db=80.0):
            """
            This function transforms the power spectrogram to decibel units.

            Source : 
            https://librosa.github.io/librosa/generated/librosa.core.power_to_db.html#librosa-core-power-to-db
            """

            ref_value = tf.reduce_max(magnitude)
            log_spec = 10.0 * tf_log10(tf.maximum(amin, magnitude))
            log_spec -= 10.0 * tf_log10(tf.maximum(amin, ref_value))
            log_spec = tf.maximum(log_spec, tf.reduce_max(log_spec) - top_db)

            return log_spec

        # Start of computation
        # Obtain the Short-Time Fourier transform
        spectrograms = tf.signal.stft(waveforms,frame_length = self.fft_size,
                                                frame_step = self.hop_length,
                                                pad_end = False)

        # Keep the magnitude
        magnitude_spectrograms = tf.abs(spectrograms)

        # Compute the Mel-Spectrograms
        mel_spectrograms = tf.matmul(   tf.square(magnitude_spectrograms), 
                                        self.mel_filterbank )

        # Scale the Mel-Spectrograms
        log_mel_spectrograms = power_to_db(mel_spectrograms)

        # Add channel dimension
        log_mel_spectrograms = tf.expand_dims(log_mel_spectrograms, 3)

        return log_mel_spectrograms

    def get_config(self):
        """ The dictionary of class configuration. """
        config = {
            'fft_size' : self.fft_size,
            'hop_length' : self.hop_length,
            'n_mels' : self.n_mels,
            'sample_rate' : self.sample_rate,
            'f_min' : self.f_min,
            'f_max' : self.f_max
        }

        config.update(super().get_config())

        return config



