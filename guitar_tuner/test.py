import matplotlib.pyplot as plt
import numpy as np
from numpy.fft import fft, fftshift

fig = plt.figure(figsize=(10, 5))
ax1 = fig.add_subplot(1, 2, 1)
ax2 = fig.add_subplot(1, 2, 2)

window = np.hanning(51)


ax1.plot(window)
ax1.set_title("Hann window")
ax1.set_ylabel("Amplitude")
ax1.set_xlabel("Sample")


A = fft(window, 2048) / 25.5
mag = np.abs(fftshift(A))
freq = np.linspace(-0.5, 0.5, len(A))
with np.errstate(divide='ignore', invalid='ignore'):
    response = 20 * np.log10(mag)

response = np.clip(response, -100, 100)
ax2.plot(freq, response, 'g')
ax2.set_title("Frequency response of the Hann window")
ax2.set_ylabel("Magnitude [dB]")
ax2.set_xlabel("Cycles per sample")
ax2.axis('tight')

plt.show()