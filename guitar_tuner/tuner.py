#! usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import pyaudio

NOTE_MIN = 40       # E2
NOTE_MAX = 64       # E4
SAMPLE_RATE = 44100       # sampling frequency in hertz
HOP_LENGTH = 2048   # samples per frame
FRAMES_PER_FFT = 16 # number of frames that FFT takes average of

# number of samples FFT will take in each step
SAMPLES_PER_FFT = HOP_LENGTH    *   FRAMES_PER_FFT
# FFT step size to process the input; 
# small value results in slower process which directly
# influenced by defined number of samples per FFT frame
FREQ_STEP       = float(SAMPLE_RATE)  /   SAMPLES_PER_FFT

# final notes print
NOTE_NAMES = 'E F F# G G# A A# B C C# D D#'.split()

def freq_to_midi(frequency): 
    """
    This function takes the pitchfrequency  and convert it to MIDI equivalent value.
    In case of a guitar tuner, note E4 = 329.63 HZ is considered as the fundamental frequency (f0),
    where the equivalent MIDI number of E4 = 64.

    Parameters:
    frequency (float) ; The pitch frequency.

    Returns: 
    MIDI number (float) : The equivalent MIDI number of pitch.
    """
    
    return 64 + 12 * np.log2(frequency / 329.63)

def midi_to_freq(midi):
    """
    This function takes the MIDI value of pitch and obtain the equivalent frequency number.
    In case of a guitar tuner, note E4 = 329.63 HZ is considered as the fundamental frequency (f0),
    where the equivalent MIDI number of E4 = 64

    Parameters:
    midi (float) ; The pitch MIDI number.

    Returns: 
    Frequency (float) : The pitch frequency.
    """

    return 329.63 * 2.0**((midi - 64) / 12.0)

def note_name(frequency):
    """
    This function takes the pitch frequency and returns the related note.

    Parameters:
    frequency (int) : The pitch frequency.

    Returns:
    note (string) : The pitch notation.
    """

    return NOTE_NAMES[frequency % NOTE_MIN % len(NOTE_NAMES)] + str(int(frequency /12 -1))

def note_to_fftbin(midi):
    """
    This function is used to get the minimum and maximum index within the FFT of pitches
    
    Parameters:
    midi (int) : The MIDI number of pitch
    FREQ_STEP (float): The FFT step size to process the input

    Returns:
    (float) : The index number of the pitch within the FFT
    """
    return midi_to_freq(midi) / FREQ_STEP

# Get the maximum and minimum index
index_min = max(0, int(np.floor(note_to_fftbin(NOTE_MIN - 1)))) 
index_max = max(SAMPLES_PER_FFT, int(np.ceil(note_to_fftbin(NOTE_MAX + 1)))) 

# Define the FFT buffer
buffer = np.zeros(SAMPLES_PER_FFT, dtype=np.float32)
num_frames = 0

# Initializing the audio stream
stream = pyaudio.PyAudio().open(format=pyaudio.paInt16,
                                channels=1,
                                rate=SAMPLE_RATE,
                                input=True,
                                frames_per_buffer=HOP_LENGTH)

stream.start_stream()

# Create Hanning window function
window = np.hanning(SAMPLES_PER_FFT)

# Print initial text
print('sampling at', SAMPLE_RATE, 'Hz with max resolution of', FREQ_STEP, 'Hz')
print('Streaming started ...')

while stream.is_active():

    # Shifting the buffer
    buffer[:-HOP_LENGTH] = buffer[HOP_LENGTH:]
    # Map new data
    buffer[-HOP_LENGTH:] = np.frombuffer(stream.read(HOP_LENGTH), np.int16)

    # Run the FFT on the windowed buffer
    fft = np.fft.rfft(buffer * window)

    # Get frequency of maximum response in range
    freq = (np.abs(fft[index_min:index_max]).argmax() + index_min) * FREQ_STEP

    # Get note's MIDI number 
    midi = freq_to_midi(freq)
    # Find the nearest note
    note_nearest = int(round(midi))

    # Count number of processed frames 
    num_frames +=1

    if num_frames >= FRAMES_PER_FFT:
        print('number {:7.2f} freq: {:7.2f} Hz     note: {:>3s} {:+.2f}'.format(midi,
                                                                                freq, note_name(note_nearest), midi - note_nearest))