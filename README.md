# Python Music Analysis #



### This is a repository for master dissertation on Music analysis and algorithmic composition of music. ###

	
* Dependencies
	
		python=3.7.5
	
		numpy
	
		matplotlib
	
		librosa
	
		scipy
	
		pyaudio 
	
		tqdm
		
		tensorflow
		
		keras
		
		tensorflow-datasets
		
* Alternatively,you can use the "enviroment.yml" from main folder to install the dependencies.

			conda env create -f environment.yml

			conda activate py_music

	* To verify the environment was installed correctly:

			conda env list
	

### Who do I talk to? ###

* Repo owner or admin
> Shayan Dadman (sda091@uit.no)
