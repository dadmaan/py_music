from mel_spectrogram import MelSpectrogram

import tensorflow as tf
import tensorflow_datasets as tfds
from tensorflow.keras.layers import (BatchNormalization, Conv2D, Dense,
                                     Dropout, Flatten, Input, MaxPool2D)
from tensorflow.keras.models import Model


def ConvModel(n_classes=11, sample_rate=16000, fft_size=1024, hop_length=512, n_mels=64, duration=4):

    n_samples = sample_rate * duration
    input_shape = (n_samples,)

    x = Input(shape=input_shape, name='input', dtype='float32')
    y0 = MelSpectrogram(sample_rate, fft_size, hop_length, n_mels, name='mel-spectrogram')(x)

    # Data normaization on frequency axis
    y1 = BatchNormalization(axis=2)(y0)

    y2 = Conv2D(32, (3, n_mels), activation='relu', name='block1_conv1')(y1)
    y3 = BatchNormalization()(y2)
    y4 = MaxPool2D((1, y3.shape[2]), name='block1_pool1')(y3)

    y5 = Conv2D(64, (3, 1), activation='relu', name='block2_conv1')(y4)
    y6 = BatchNormalization()(y5)
    y7 = MaxPool2D(pool_size=(2, 1), name='block2_pool')(y6)

    y8 = Flatten()(y7)
    y9 = Dense(256, activation='relu', name='fc1')(y8)
    y10 = Dropout(0.5)(y9)
    y = Dense(n_classes, activation='softmax', name='prediction')(y10)

    return Model(inputs=x, outputs=y)