from mel_spectrogram import MelSpectrogram

import tensorflow as tf
import tensorflow_datasets as tfds
from tensorflow.keras.layers import (BatchNormalization, Conv2D, Dense,
                                     Dropout, Flatten, Input, MaxPool2D,
                                    ZeroPadding2D, GlobalMaxPooling2D)
from tensorflow.keras.models import Model


def ConvModel(n_classes=11, sample_rate=16000, fft_size=1024, hop_length=512, n_mels=128, duration=4):

    n_samples = sample_rate * duration
    input_shape = (n_samples,)

    x = Input(shape=input_shape, name='input', dtype='float32')
    y0 = MelSpectrogram(sample_rate, fft_size, hop_length, n_mels, name='mel-spectrogram')(x)

    # Data normaization on frequency axis
    y1 = BatchNormalization(axis=2)(y0)

    y2 = ZeroPadding2D(padding=(1, 1), input_shape=input_shape)(y1)
    y3 = Conv2D(32, 3, 3, activation='relu', kernel_initializer='glorot_uniform', name='block1_conv1')(y2)
    y4 = BatchNormalization()(y3)
    y5 = MaxPool2D((2, 2), strides=(1, 1) , name='block1_pool1')(y4)

    y6 = Dropout(0.25)(y5)
    
    y7 = ZeroPadding2D(padding=(1, 1))(y6)
    y8 = Conv2D(64, 3, 3, activation='relu', kernel_initializer='glorot_uniform', name='block2_conv1')(y7)
    y9 = BatchNormalization()(y8)
    y10 = GlobalMaxPooling2D()(y9)
    
    y11 = Flatten()(y10)
    
    y12 = Dense(256, activation='relu', name='fc1')(y11)
    
    y13 = Dropout(0.5)(y12)
    
    y = Dense(n_classes, activation='sigmoid', name='prediction')(y13)

    return Model(inputs=x, outputs=y)