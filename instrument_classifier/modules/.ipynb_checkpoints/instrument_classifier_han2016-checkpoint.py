from mel_spectrogram import MelSpectrogram

import tensorflow as tf
import tensorflow_datasets as tfds
from tensorflow.keras.layers import (BatchNormalization, Conv2D, Dense,
                                     Dropout, Flatten, Input, MaxPool2D, 
                                     ZeroPadding2D, GlobalMaxPooling2D)
from keras.layers.advanced_activations import LeakyReLU
from tensorflow.keras.models import Model


def ConvModel(n_classes=11, sample_rate=16000, fft_size=1024, hop_length=512, n_mels=128, duration=4):

    n_samples = sample_rate * duration
    input_shape = (n_samples,)

    x = Input(shape=input_shape, name='input', dtype='float32')
    y0 = MelSpectrogram(sample_rate, fft_size, hop_length, n_mels, name='mel_spectrogram')(x)

    y1 = ZeroPadding2D(padding=(1, 1), input_shape=input_shape)(y0)

    y2 = Conv2D(32, 3, 3, 
                padding='same', 
                kernel_initializer='glorot_uniform', 
                name='block1_conv1')(y1)
    LeakyReLU(alpha=0.33)
    y3 = ZeroPadding2D(padding=(1, 1))(y2)
    y4 = Conv2D(32, 3, 3, 
                padding='same', 
                kernel_initializer='glorot_uniform', 
                name='block1_conv2')(y3)
    LeakyReLU(alpha=0.33)
    y5 = MaxPool2D((3, 3), strides=(3, 3), name='block1_pool')(y4)
    
    y6 = Dropout(0.25)(y5)
    
    y7 = ZeroPadding2D(padding=(1, 1))(y6)
    y8 = Conv2D(64, 3, 3, 
                 padding='same', 
                 kernel_initializer='glorot_uniform', 
                 name='block2_conv1')(y7)
    LeakyReLU(alpha=0.33)
    y9 = ZeroPadding2D(padding=(1, 1))(y8)
    y10 = Conv2D(64, 3, 3, 
                 padding='same', 
                 kernel_initializer='glorot_uniform', 
                 name='block2_conv2')(y9)
    LeakyReLU(alpha=0.33)
    y11 = MaxPool2D((1, 1), strides=(1, 1), name='block2_pool')(y10)
    
    y12 = Dropout(0.25)(y11)
    
    y13 = ZeroPadding2D(padding=(1, 1))(y12)
    y14 = Conv2D(128, 3, 3, 
                 padding='same', 
                 kernel_initializer='glorot_uniform',
                 name='block3_conv1')(y13)
    LeakyReLU(alpha=0.33)
    y15 = ZeroPadding2D(padding=(1, 1))(y14)
    y16 = Conv2D(128, 3, 3, 
                 padding='same', 
                 kernel_initializer='glorot_uniform', 
                 name='block3_conv2')(y15)
    LeakyReLU(alpha=0.33)
    y17 = MaxPool2D((1, 1), strides=(1, 1), name='block3_pool')(y16)
    
    y18 = Dropout(0.25)(y17)
    
    y19 = ZeroPadding2D(padding=(1, 1))(y18)
    y20 = Conv2D(256, 3, 3, 
                 padding='same', 
                 kernel_initializer='glorot_uniform', 
                 name='block4_conv1')(y19)
    LeakyReLU(alpha=0.33)
    y21 = ZeroPadding2D(padding=(1, 1))(y20)
    y22 = Conv2D(256, 3, 3, padding='same', 
                 kernel_initializer='glorot_uniform', 
                 name='block4_conv2')(y21)
    LeakyReLU(alpha=0.33)
    y23 = MaxPool2D((1, 1), strides=(1, 1), name='block4_pool')(y22)
    
    y24 = GlobalMaxPooling2D()(y23)

#     y33 = Flatten()(y32)
    y25 = Dense(1024, activation='relu', name='fc1')(y24)
    LeakyReLU(alpha=0.33)
    y26 = Dropout(0.5)(y25)
    
    y = Dense(n_classes, activation='sigmoid', name='prediction')(y26)

    return Model(inputs=x, outputs=y)