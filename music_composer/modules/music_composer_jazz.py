import tensorflow as tf
from tensorflow.keras.layers import (BatchNormalization, Dense,
                                     Dropout, Flatten, Input,
                                     TimeDistributed, LSTM, Embedding, Activation)
from tensorflow.keras.models import Model, Sequential

def LSTMModel(vocab_size, batch_size, embedding_dim=512):
    
    model = Sequential()
    
    model.add(Embedding(input_dim = vocab_size, 
                        output_dim = embedding_dim, 
                        batch_input_shape = [batch_size, None],
                       name='embed1')) 
    
    model.add(LSTM(256, 
                   return_sequences = True, 
                   stateful = True,
                   name='lstm1'))
    
    model.add(Dropout(0.2))
    
    model.add(LSTM(256, 
                   return_sequences = True, 
                   stateful = True,
                   name='lstm2'))
    
    model.add(Dropout(0.2))
    
    model.add(LSTM(256, 
                   return_sequences = True, 
                   stateful = True,
                   name='lstm3'))
    
    model.add(Dropout(0.2))
    
    model.add(TimeDistributed(Dense(vocab_size, activation='softmax'), name='time_dist1'))
    
    return model
    
    
def composer(vocab_size, batch_size, embedding_dim=512):
    
    model = Sequential()
    
    model.add(Embedding(input_dim = vocab_size, 
                        output_dim = embedding_dim, 
                        batch_input_shape = [batch_size, None],
                       name='embed1')) 
    
    model.add(LSTM(256, 
                   return_sequences = True, 
                   stateful = True,
                   name='lstm1'))
    
    model.add(Dropout(0.2))
    
    model.add(LSTM(256, 
                   return_sequences = True, 
                   stateful = True,
                   name='lstm2'))
    
    model.add(Dropout(0.2))
    
    model.add(LSTM(256, 
                   return_sequences = False, 
                   stateful = True,
                   name='lstm3'))
    
    model.add(Dropout(0.2))
    
    model.add(Dense(vocab_size, activation='softmax', name='prediction'))
    
    return model