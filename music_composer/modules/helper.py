import os
import tensorflow as tf
import pandas as pd
import numpy as np
from keras.callbacks import Callback
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score


def save_training_history(path, training_history, f1_scores=None, plot=False):
    log_path = path + '.csv'
    
    if os.path.exists(log_path):
        training_logs = pd.read_csv(log_path)
        
        current_training_log = pd.DataFrame(training_history)
        if not(f1_scores == None):    
            current_training_log.loc[:,'f1_score'] = pd.Series(f1_scores, 
                                                    index=current_training_log.index)
        
        training_logs = pd.concat([training_logs, current_training_log], 
                                  ignore_index=True)
        training_logs.to_csv(log_path, index=False)
    
    else:
        training_logs = pd.DataFrame(training_history)
        if not(f1_scores == None):    
            training_logs.loc[:,'f1_score'] = pd.Series(f1_scores, 
                                                    index=training_logs.index)
        training_logs.to_csv(log_path, index=False)
    
    if plot ==True:
        # Plot training & validation accuracy values
        plt.figure(figsize=(10, 8))
        plt.plot(training_history['accuracy'])
        if 'val_accuracy' in training_history:
            plt.plot(training_history['val_accuracy'])
            plt.legend(['Train', 'Valid'], loc='upper left')
        plt.title('Model accuracy')
        plt.ylabel('Accuracy')
        plt.xlabel('Epoch')
        plt.legend(['Train'], loc='upper left')
        plt.savefig(path + '_acc.png')
        plt.show()

        # Plot training & validation loss values
        plt.figure(figsize=(10, 8))
        plt.plot(training_history['loss'])
        if 'val_loss' in training_history:
            plt.plot(training_history['val_loss'])
            plt.legend(['Train', 'Valid'], loc='upper left')
        plt.title('Model loss')
        plt.ylabel('Loss')
        plt.xlabel('Epoch')
        plt.legend(['Train'], loc='upper left')
        plt.savefig(path + '_loss.png')
        plt.show()
    
class F1Score(Callback):
    def __init__(self, valid_data, average='samples'):
        super(Callback, self).__init__()
        self.validation_data = valid_data
        self.val_f1s = []
        self.val_recalls = []
        self.val_precisions = []
        self.average = average

    def on_epoch_end(self, epoch, logs={}):
        val_predict = (np.asarray(self.model.predict(self.validation_data[0]))).round()
        val_targ = self.validation_data[1]
        _val_f1 = f1_score(val_targ, val_predict, average=self.average)
        _val_recall = recall_score(val_targ, val_predict, average=self.average)
        _val_precision = precision_score(val_targ, val_predict, average=self.average)
        self.val_f1s.append(_val_f1)
        self.val_recalls.append(_val_recall)
        self.val_precisions.append(_val_precision)
        print (" val_f1: %f  val_precision: %f  val_recall %f" %(_val_f1, _val_precision, _val_recall))
        return

    

    
#     To-Do
# class BaseLinePrediction():
    
#     def create_time_steps(length):
#         return list(range(-length, 0))
    
#     def show_plot(plot_data, delta, title):
#         labels = ['History', 'True Future', 'Model Prediction']
#         marker = ['.-', 'rx', 'go']
#         time_steps = create_time_steps(plot_data[0].shape[0])
#         if delta:
#             future = delta
#         else:
#             future = 0

#         plt.title(title)
#         for i, x in enumerate(plot_data):
#             if i:
#                 plt.plot(future, plot_data[i], marker[i], markersize=10,
#                            label=labels[i])
#         else:
#             plt.plot(time_steps, plot_data[i].flatten(), marker[i], label=labels[i])
#         plt.legend()
#         plt.xlim([time_steps[0], (future+5)*2])
#         plt.xlabel('Time-Step')
#         return plt
    
#     def baseline(history):
#         return np.mean(history)
        
#     def plot_prediction():
#         x = np.array(X_train)
#         y = np.array(y_train)
#         show_plot([x, y, baseline(x)], 0, 'Baseline Prediction Example')