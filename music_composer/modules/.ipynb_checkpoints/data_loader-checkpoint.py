import os
import tensorflow as tf
import numpy as np
import json
import random

AUTOTUNE = tf.data.experimental.AUTOTUNE


def parse_batch(record_batch, sample_rate, duration):
    """ Parse each batch into a dataset of [audio, label] pairs """
    n_samples = sample_rate * duration

    # Create a description of the features
    feature_description = {
        'audio': tf.io.FixedLenFeature([n_samples], tf.float32),
        'label': tf.io.FixedLenFeature([8], tf.int64),
    }

    # Parse the input `tf.Example` proto using the dictionary above
    example = tf.io.parse_example(record_batch, feature_description)

    return example['audio'], example['label']
    
def get_dataset_from_tfrecords(tfrecords_dir='/run/media/dadmaan/Seagate Backup Plus Drive/datasets/fma/tfrecords/10sec',
                                split='train',
                                batch_size=64, 
                                sample_rate=22050, 
                                duration=10,
                                epoch=20):
    """ Locate the tfrecord files and prepare the corresponding dataset.
        Pass the split name to generate each of the train/test/validation dataaset.
        
        Parameters:
        tfrecords_dir: direction of tfrecord file.
        split : refers to desired train/test/valid dataset to generate.
        batch_size: number of desired batches.
        buffer_size: size of buffer for shuffling dataset.
        sample_rate: sample_rate of audio file to generate dataset.
        duration: duration of audio file to generate dataset.
        epoch: number of iteration on whole dataset.
        
        Return:
        Tensorflow dataset.
    """
    if split not in ('train', 'test', 'valid'):
        raise ValueError("split must be either 'train', 'test' or 'validate'")

    # List all *.tfrecord files for the selected split
    pattern = os.path.join(tfrecords_dir, '{}*.tfrec'.format(split))
    files_ds = tf.data.Dataset.list_files(pattern)

    # Disregard data order in favor of reading speed
    ignore_order = tf.data.Options()
    ignore_order.experimental_deterministic = False
    files_ds = files_ds.with_options(ignore_order)

    # Read TFRecord files in an interleaved order
    ds = tf.data.TFRecordDataset(files_ds,
                                 compression_type='ZLIB',
                                 num_parallel_reads=AUTOTUNE)
    # Prepare batches
    ds = ds.batch(batch_size)

    # Parse a batch into a dataset of [audio, label] pairs
    ds = ds.map(lambda x: parse_batch(x, sample_rate, duration))

    # Repeat the training data for n_epochs. Don't repeat test/validate splits.
    if split == 'train':
        ds = ds.repeat(epoch)

    return ds.prefetch(buffer_size=AUTOTUNE)

class ABCDatasetLoader():
    """ ABC music notation dataset loader"""
    
    def __init__(self,
                 sequence_length,
                 batch_size,
                 buffer_size,
                 text_dir='dataset/abc/tunes.txt',
                 output_dir='dataset/abc/vocab_tunes.json',
                 dataset_size='full'):
        
        self.sequence_length = sequence_length
        self.batch_size = batch_size
        self.buffer_size = buffer_size
        self.text_dir = text_dir
        self.output_dir = output_dir
        self.vocab_length = 0
        self.char2idx = {}
        self.idx2char = []
        self.dataset_length = 0
        self.ds_size = dataset_size
    
    def unique_chars(self, dataset):
        """ Determines the unique character in the dataset """
        return sorted(list(set(dataset)))

    def get_split(self, dataset, split):
        """ returns the trainig/validation/testing examples """
        if split == 'train':
            length = int(self.dataset_length * 0.8)
            return dataset[:length]
        if split == 'valid':
            begin = int(self.dataset_length * 0.8)
            end = int(self.dataset_length *0.9)
            return dataset[begin:end]
        if split == 'test':
            begin = int(self.dataset_length *0.9)
            end = int(self.dataset_length)
            return dataset[begin:end]
        
    def char2index(self, vocab):
        """ Map the vocabulary characters to corresponding index"""
        c2i = {ch : i for i,ch in enumerate(vocab)}
        return c2i

    def index2char(self, vocab):
        """ Map the indices to corresponding characters in vocabulary"""
        return np.array(vocab)

    def split_input_target(self, chunk):
        """ Split the dataset into chunks of input/target for each dataset.
            Simply duplicate the given sequence and shift it. 
        """
        input_text = chunk[:-1]
        target_text = chunk[1:]
        return input_text, target_text

    def load_vocab(self, path):
        """ Load the vocab file """
        with open(path) as f:
            vocab = json.load(f)
        return vocab
    
    def get_vocab_length(self):
        return self.vocab_length
    
    def get_char2index(self):
        return self.char2idx
    
    def get_index2char(self):
        return self.idx2char
    
    def get_stepsize(self):
        return int(self.dataset_length / (self.sequence_length * self.batch_size))
    
    def get_dataset_length(self):
        return self.dataset_length
    
    def get_subdataset(self, dataset, size):
        """ Reduces size of the dataset """
        if size == 'small':
            length = int(len(dataset)//6)
            dataset = dataset[:length]
            self.dataset_length = len(dataset)
            return dataset
        
        if size == 'medium':
            length = int(len(dataset)//4)
            dataset = dataset[:length]
            self.dataset_length = len(dataset)
            return dataset
        
        if size == 'large':
            length = int(len(dataset)//2)
            dataset = dataset[:length]
            self.dataset_length = len(dataset)
            return dataset
        
    def get_abc_dataset_from_text(self, split=None):
        """ Get the music text file in ABC notation, 
            prepare the training examples/targets with tensorflow tf.dataset for RNN model.
            Here, we generate each batch that contains sequence of examples with the same length
            except one character shifted to the right

            Return:
            Tensorflow dataset
        """

        # Fetch the text file
        text = open(self.text_dir, 'r').read()
        self.dataset_length = len(text)

        # Get the unique characters
        vocab = self.unique_chars(text)
        self.vocab_length = len(vocab)

        # Save the unique characters
        with open(self.output_dir, 'w') as f:
            json.dump(vocab, f)

        # Map unique characters to indices
        self.char2idx = self.char2index(vocab)
        # Map indices to unique characters
        self.idx2char = self.index2char(vocab)

        # Map the dataset characters to int value
        text_as_int = np.array([self.char2idx[c] for c in text])
        
        # Define the size of dataset
        if not(self.ds_size == 'full'):
            text_as_int = self.get_subdataset(text_as_int, self.ds_size)
        
        # Get the right split from the dataset
        if not(split == None):
            text_as_int = self.get_split(text_as_int, split)
        
        # Preparing the tensorflow pipeline
        char_dataset = tf.data.Dataset.from_tensor_slices(text_as_int)

        # Create the sequence of examples; each sequence shifted one character
        sequences = char_dataset.batch(self.sequence_length + 1, drop_remainder=True)

        # Parse each batch into a dataset of [input, target] example pairs
        ds = sequences.map(self.split_input_target)

        # Shuffle the dataset 
        ds = ds.shuffle(self.buffer_size)

        # Create the batches
        ds = ds.batch(self.batch_size, drop_remainder=True)

        return ds
    
    