# Source : https://sudonull.com/post/61595-Hessian-Free-optimization-with-TensorFlow
# Source Code : https://github.com/MoonL1ght/HessianFreeOptimization

import tensorflow as tf

def __conjugate_gradient(self, gradients):
    """ 
        Performs conjugate gradient method to minimze quadratic equation 
        and find best delta of network parameters.
        
        gradients: list of Tensorflow tensor objects
            Network gradients.
            
        return: Tensorflow tensor object
            Update operation for delta.
            
        return: Tensorflow tensor object
            Residual norm, used to prevent numerical errors. 
            
        return: Tensorflow tensor object
            Delta loss. 
    """
    with tf.name_scope('conjugate_gradient'):
        cg_update_ops = []
        prec = None

        # calculation of the matrix P by the formula (9)
        if self.use_prec:
            if self.prec_loss is None:
                graph = tf.get_default_graph()
                lop = self.loss.op.node_def
                self.prec_loss = graph.get_tensor_by_name(lop.input[0] + ':0')
            batch_size = None

            if self.batch_size is None:
                self.prec_loss = tf.unstack(self.prec_loss)
                batch_size = self.prec_loss.get_shape()[0]

            else:
                self.prec_loss = [tf.gather(self.prec_loss, i) 
                                  for i in range(self.batch_size)]
                batch_size = len(self.prec_loss)

            prec = [[g**2 for g in tf.gradients(tf.gather(self.prec_loss, i),
                                                self.W)] for i in range(batch_size)]
            prec = [(sum(tensor) + self.damping)**(-0.75)
                    for tensor in np.transpose(np.array(prec))]

        # basic conjugate gradient algorithm
        Ax = None
        if self.use_gnm:
            Ax = self.__Gv(self.cg_delta)
        else:
            Ax = self.__Hv(gradients, self.cg_delta)

        b = [-grad for grad in gradients]
        bAx = [b - Ax for b, Ax  in zip(b, Ax)]
        condition = tf.equal(self.cg_step, 0)
        r = [tf.cond(condition, lambda: tf.assign(r,  bax),
                                lambda: r) for r, bax  in zip(self.residuals, bAx)]

        d = None
        if self.use_prec:
            d = [tf.cond(condition, lambda: tf.assign(d, p * r), 
                                    lambda: d) for  p, d, r in zip(prec, self.directions, r)]
        else:
            d = [tf.cond(condition, lambda: tf.assign(d, r), 
                                    lambda: d) for  d, r in zip(self.directions, r)]

        Ad = None
        if self.use_gnm:
            Ad = self.__Gv(d)
        else:
            Ad = self.__Hv(gradients, d)

        residual_norm = tf.reduce_sum([tf.reduce_sum(r**2) 
                                       for r in r])
        alpha = tf.reduce_sum([tf.reduce_sum(d * ad) 
                               for d, ad in zip(d, Ad)])
        alpha = residual_norm / alpha

        if self.use_prec:
        beta = tf.reduce_sum([tf.reduce_sum(p * (r - alpha * ad)**2)
                              for r, ad, p in zip(r, Ad, prec)])
        else:
            beta = tf.reduce_sum([tf.reduce_sum((r - alpha * ad)**2) 
                                  for r, ad in zip(r, Ad)])
        self.beta = beta
        beta = beta / residual_norm
        for i, delta  in reversed(list(enumerate(self.cg_delta))):

            update_delta = tf.assign(delta, 
                                     delta + alpha * d[i], 
                                     name='update_delta')

            update_residual = tf.assign(self.residuals[i], 
                                    r[i] - alpha * Ad[i], 
                                    name='update_residual')
            p = 1.0
            if self.use_prec:
                p = prec[i]

            update_direction = tf.assign(self.directions[i], 
                                         p * (r[i] - alpha * Ad[i]) + beta * d[i], 
                                         name='update_direction')

            cg_update_ops.append(update_delta)
            cg_update_ops.append(update_residual)
            cg_update_ops.append(update_direction)

        with tf.control_dependencies(cg_update_ops):
            cg_update_ops.append(tf.assign_add(self.cg_step, 1))

    cg_op = tf.group(*cg_update_ops)
    dl = tf.reduce_sum([tf.reduce_sum(0.5*(delta*ax) + grad*delta)
                        for delta, grad, ax in zip(self.cg_delta, gradients, Ax)])

    return cg_op, residual_norm, dl


def __Hv(self, grads, vec):
    """ 
        Computes Hessian vector product.
        
        grads: list of Tensorflow tensor objects
            Network gradients.
        vec: list of Tensorflow tensor objects
            Vector that is multiplied by the Hessian.
            
        return: list of Tensorflow tensor objects
            Result of multiplying Hessian by vec. 
    """
    grad_v = [tf.reduce_sum(g * v) for g, v in zip(grads, vec)]
    Hv = tf.gradients(grad_v, self.W, stop_gradients=vec)
    Hv = [hv + self.damp_pl * v for hv, v in zip(Hv, vec)]
    return Hv

def __Rop(self, f, x, vec):
    """ 
        Computes Jacobian vector product.
        
        f: Tensorflow tensor object
            Objective function.
        x: list of Tensorflow tensor objects
            Parameters with respect to which computes Jacobian matrix.
        vec: list of Tensorflow tensor objects
            Vector that is multiplied by the Jacobian.
            
        return: list of Tensorflow tensor objects
            Result of multiplying Jacobian (df/dx) by vec. 
    """
    r = None
    if self.batch_size is None:
        try:
            r = [tf.reduce_sum([tf.reduce_sum(v * tf.gradients(f, x)[i])
                                for i, v in enumerate(vec)])
                 for f in tf.unstack(f)]
        except ValueError:
            assert False, clr.FAIL + clr.BOLD + 'Batch size is None, but used '\
              'dynamic shape for network input, set proper batch_size in '\
              'HFOptimizer initialization' + clr.ENDC
    else:
        r = [tf.reduce_sum([tf.reduce_sum(v * tf.gradients(tf.gather(f, i), x)[j])
                            for j, v in enumerate(vec)])
             for i in range(self.batch_size)]
    
    assert r is not None, clr.FAIL + clr.BOLD +\
        'Something went wrong in Rop computation' + clr.ENDC
    
    return r


def __Gv(self, vec):
    """ 
        Computes the product G by vec = JHJv (G is the Gauss-Newton matrix).
        vec: list of Tensorflow tensor objects
            Vector that is multiplied by the Gauss-Newton matrix.
            
        return: list of Tensorflow tensor objects
            Result of multiplying Gauss-Newton matrix by vec. 
    """
    Jv = self.__Rop(self.output, self.W, vec)
    Jv = tf.reshape(tf.stack(Jv), [-1, 1])
    HJv = tf.gradients(tf.matmul(tf.transpose(tf.gradients(self.loss,self.output)[0]), Jv), 
                       self.output, stop_gradients=Jv)[0]
    JHJv = tf.gradients(tf.matmul(tf.transpose(HJv), self.output), 
                        self.W, stop_gradients=HJv)
    JHJv = [gv + self.damp_pl * v for gv, v in zip(JHJv, vec)]
    
    return JHJv


def minimize(self, feed_dict, debug_print=False):
    """ 
        Performs main training operations.
        feed_dict: dictionary
            Input training batch.
        debug_print: bool
            If True prints CG iteration number. 
    """
    self.sess.run(tf.assign(self.cg_step, 0))
    feed_dict.update({self.damp_pl:self.damping})
    
    if self.adjust_damping:
        loss_before_cg = self.sess.run(self.loss, feed_dict)
        
    dl_track = [self.sess.run(self.ops['dl'], feed_dict)]
    self.sess.run(self.ops['set_delta_0'])
    
    for i in range(self.cg_max_iters):
        if debug_print:
            d_info = clr.OKGREEN + '\r[CG iteration: {}]'.format(i) + clr.ENDC
            sys.stdout.write(d_info)
            sys.stdout.flush()
        k = max(self.gap, i // self.gap)
        rn = self.sess.run(self.ops['res_norm'], feed_dict)
        # early stop to prevent numerical error
        if rn < self.cg_num_err:
            break
        self.sess.run(self.ops['cg_update'], feed_dict)
        dl_track.append(self.sess.run(self.ops['dl'], feed_dict))
        # early stop of the algorithm based on formula (3)
        if i > k:
            stop = (dl_track[i+1] - dl_track[i+1-k]) / dl_track[i+1]
            
        if not np.isnan(stop) and stop < 1e-4:
            break
            
        if debug_print:
            sys.stdout.write('\n')
            sys.stdout.flush()
            
        if self.adjust_damping:
            feed_dict.update({self.damp_pl:0})
            dl = self.sess.run(self.ops['dl'], feed_dict)
            feed_dict.update({self.damp_pl:self.damping})
            
    self.sess.run(self.ops['train'], feed_dict)
    if self.adjust_damping:
        loss_after_cg = self.sess.run(self.loss, feed_dict)
        # reduction ratio (7)
        reduction_ratio = (loss_after_cg - loss_before_cg) / dl
        # heuristics in Levenberg-Markward (8)
        if reduction_ratio < 0.25 and self.damping > self.damp_num_err:
            self.damping *= 1.5
        elif reduction_ratio > 0.75 and self.damping > self.damp_num_err:
            self.damping /= 1.5