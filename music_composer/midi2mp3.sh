#!/bin/bash

echo To use this script you need to have the following packages on your system
echo FluidSynth, SoundFonts, TwoLame.

echo Ok then. Lets convert your MIDI to MP3!
echo Now, give me some info

read -p 'MIDI file path : ' input_path
read -p 'Output directory :' output_path


fluidsynth -l -T raw -F - /usr/share/soundfonts/FluidR3_GM.sf2 $input_path | twolame -b 256 -r - $output_path

echo All done
